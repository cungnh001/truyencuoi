package com.cungnh.truyencuoi.entity;

public class StoryEntity {
    private String name, content;
    private  boolean favorite;

    public StoryEntity(String name, String content,boolean favorite ) {
        this.name = name;
        this.content = content;
        this.favorite = favorite;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean getFavorite(){
        return favorite;
    }

    @Override
    public String toString() {
        return "Name: " + name + " \nFavorite : " + favorite + "\n" + "Content: " + content ;
    }
}
