package com.cungnh.truyencuoi;

import android.view.Gravity;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.cungnh.truyencuoi.adapter.StoryVPAdapter;
import com.cungnh.truyencuoi.entity.StoryEntity;

import java.text.MessageFormat;
import java.util.List;

public class SaveStory extends BaseActivity implements StoryVPAdapter.OnItemCallBack {
    private int topicPosition;
    private int storyPosition;
    private List<StoryEntity> listData;
    private TextView tvPage;
    private ViewPager viewPager;
    private StoryVPAdapter mAdapter;
    private DrawerLayout drawerLayout;

    @Override
    protected void initView() {
        topicPosition = CommonUtils.getInstance().getPositionTopic();

        storyPosition = CommonUtils.getInstance().getPositionStory();

        TextView tvTopic = findViewById(R.id.tv_content_topic_actionbar);
        listData = CommonUtils.getInstance().getStory(topicPosition);

        tvTopic.setText(MainStoryAct.NAME_TOPIC[topicPosition]);

        tvPage = findViewById(R.id.tv_page);
        tvPage.setText(storyPosition + 1 + "/" + listData.size());

        viewPager = findViewById(R.id.vp_story);
        drawerLayout = findViewById(R.id.drawer_layout1);
//        drawerLayout.addDrawerListener(this);
//        linearLayout = findViewById(R.id.linear_view);

//        sc = findViewById(R.id.sc_night);
//        sc.setChecked(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES);
//        sc.setOnCheckedChangeListener(this);

        findViewById(R.id.iv_back, this);
        findViewById(R.id.iv_save, this);
        findViewById(R.id.tv_save, this);
        findViewById(R.id.iv_menu, this);
        findViewById(R.id.tv_menu, this);
        findViewById(R.id.iv_favorite, this);


        mAdapter = new StoryVPAdapter(this, this, listData);

        viewPager.addOnPageChangeListener(new StoryPageChangeAdapter() {
            @Override
            public void onPageSelected(int position) {
                tvPage.setText(MessageFormat.format("{0}/{1}", position + 1, listData.size()));
            }
        });

        viewPager.setAdapter(mAdapter);
//        initData(isSave);

        viewPager.setCurrentItem(storyPosition);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.list_story_content;
    }

    @Override
    public void clickItem(StoryEntity storyEntity) {

    }

    @Override
    public void clickView(int id) {
        if (id == R.id.iv_save || id == R.id.tv_save) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            CommonUtils.getInstance().savePositionStory(viewPager.getCurrentItem(), App.getInstance().getTopicPosition());
//            Toast.makeText(this, "Save= " + isSave, Toast.LENGTH_SHORT).show();
        } else if (id == R.id.iv_menu || id == R.id.tv_menu) {
            drawerLayout.closeDrawer(Gravity.LEFT);
//            startActivity(new Intent(this, MainStoryAct.class));
            finish();
        } else if (id == R.id.iv_back) {
//            App.getInstance().setTopicPosition(topicPosition);
//            startActivity(new Intent(this, ListStory.class));
            finish();
        }
    }
}
