package com.cungnh.truyencuoi;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, Animation.AnimationListener {
    private Animation anim;
    private boolean isStartAnim;
    private int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        anim = new AnimationUtils().loadAnimation(this, R.anim.alpha_anim);
        anim.setAnimationListener(this);

        initView();
    }

    protected abstract void initView();

    protected abstract int getLayoutId();

    public <T extends View> T findViewById(int id, View.OnClickListener event) {
        T v = findViewById(id);
        if (v != null && event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    public String textOf(TextView v) {
        if (v == null) return null;
        return v.getText().toString();
    }

    @Override
    public final void onClick(View v) {
        if (!isStartAnim) {
            id = v.getId();
            isStartAnim = true;
            v.startAnimation(anim);
        }

    }

    protected void clickView(int id) {
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        isStartAnim = false;
        clickView(id);

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
