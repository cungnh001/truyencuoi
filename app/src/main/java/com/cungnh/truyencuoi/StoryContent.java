package com.cungnh.truyencuoi;

import android.content.Intent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.cungnh.truyencuoi.adapter.StoryVPAdapter;
import com.cungnh.truyencuoi.entity.StoryEntity;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class StoryContent extends BaseActivity implements CompoundButton.OnCheckedChangeListener, DrawerLayout.DrawerListener, StoryVPAdapter.OnItemCallBack {

    private static final String TAG = StoryContent.class.getName();
    private static final int LEVEL_LIKE = 1;
    private static final int LEVEL_UNLIKE = 0;
    private List<StoryEntity> listData = new ArrayList<>();
    public static List<StoryEntity> listFavorite = new ArrayList<>();
    private TextView tvPage;
    private StoryVPAdapter mAdapter;
    private ViewPager mVpStory;
    private boolean isSave;
    private int topicPosition, storyPosition;
    private DrawerLayout drawerLayout;
    private LinearLayout linearLayout;
    private SwitchCompat sc;
    private ImageView ivFavorite;


    @Override
    protected int getLayoutId() {
        return R.layout.list_story_content;
    }

    @Override
    protected void initView() {
        topicPosition = App.getInstance().getTopicPosition();
        storyPosition = App.getInstance().getStoryPosition();
        isSave = CommonUtils.getInstance().getIsSave();

        TextView tvTopic = findViewById(R.id.tv_content_topic_actionbar);
        listData = CommonUtils.getInstance().getStory(topicPosition);

        tvTopic.setText(MainStoryAct.NAME_TOPIC[topicPosition]);

        tvPage = findViewById(R.id.tv_page);
        tvPage.setText(storyPosition + 1 + "/" + listData.size());

        mVpStory = findViewById(R.id.vp_story);
        drawerLayout = findViewById(R.id.drawer_layout1);
        drawerLayout.addDrawerListener(this);
        linearLayout = findViewById(R.id.linear_view);

        sc = findViewById(R.id.sc_night);
        sc.setChecked(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES);
        sc.setOnCheckedChangeListener(this);

        findViewById(R.id.iv_back, this);
        findViewById(R.id.iv_save, this);
        findViewById(R.id.tv_save, this);
        findViewById(R.id.iv_menu, this);
        findViewById(R.id.tv_menu, this);
//        findViewById(R.id.iv_favorite, this);

//        ivFavorite = findViewById(R.id.iv_favorite);

        mAdapter = new StoryVPAdapter(this, this, listData);

        mVpStory.addOnPageChangeListener(new StoryPageChangeAdapter() {
            @Override
            public void onPageSelected(int position) {
                tvPage.setText(MessageFormat.format("{0}/{1}", position + 1, listData.size()));
            }
        });

        mVpStory.setAdapter(mAdapter);
//        initData(isSave);

        mVpStory.setCurrentItem(storyPosition);

    }


    private void initData(boolean isSave) {
        int topicPos = CommonUtils.getInstance().getPositionTopic();
        int currentStoryPos = CommonUtils.getInstance().getPositionStory();
        if (isSave) {
            listData = CommonUtils.getInstance().getStory(topicPos);
            mAdapter.notifyDataSetChanged();
            tvPage.setText(MessageFormat.format("{0}/{1}", currentStoryPos + 1, listData.size()));
            mVpStory.setCurrentItem(currentStoryPos);
        } else {
            tvPage.setText(MessageFormat.format("{0}/{1}", 1, listData.size()));
        }

//        Toast.makeText(this,   topicPos+" : " + currentStoryPos, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void clickView(int id) {
        if (id == R.id.iv_save || id == R.id.tv_save) {
//            isSave = !isSave;
            drawerLayout.closeDrawer(linearLayout);
//            CommonUtils.getInstance().setIsSave(isSave);
            CommonUtils.getInstance().savePositionStory(mVpStory.getCurrentItem(), App.getInstance().getTopicPosition());
            Toast.makeText(this, "Save= " + isSave, Toast.LENGTH_SHORT).show();
        } else if (id == R.id.iv_menu || id == R.id.tv_menu) {
            drawerLayout.closeDrawer(linearLayout);
            startActivity(new Intent(this, MainStoryAct.class));
            finish();
        } else if (id == R.id.iv_back) {
            onBackPressed();
            finish();
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        drawerLayout.closeDrawers();
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {
        AppCompatDelegate.setDefaultNightMode(sc.isChecked() ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }


//    @Override
//    public void clickItem(StoryEntity storyEntity) {
////        ivFavorite.setImageLevel(ivFavorite.getDrawable().getLevel() == LEVEL_UNLIKE ? LEVEL_LIKE : LEVEL_UNLIKE);
////        storyEntity.setFavorite(true);
//        listFavorite.add(storyEntity);
//        Toast.makeText(StoryContent.this, "Name: " + storyEntity.getName() + "/ " +
//                storyEntity.getFavorite(), Toast.LENGTH_SHORT).show();
//    }


    @Override
    public void clickItem(StoryEntity storyEntity) {
        listFavorite.add(storyEntity);
        Toast.makeText(StoryContent.this, "Name: " + storyEntity.getName() + "/ " +
                storyEntity.getFavorite(), Toast.LENGTH_SHORT).show();

    }
}

