package com.cungnh.truyencuoi;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cungnh.truyencuoi.adapter.StoryRVAdapter;
import com.cungnh.truyencuoi.entity.StoryEntity;

import java.util.List;

public class ListFavoriteStory extends BaseActivity implements StoryRVAdapter.OnItemCallBackRV {
    private LinearLayout lnListStory;
    private List<StoryEntity> listStory;
    private int pos;
    private StoryRVAdapter rvAdapter;
    private RecyclerView rvStory;


    @Override
    protected int getLayoutId() {
        return R.layout.list_favorite_story;
    }

    @Override
    protected void initView() {
        findViewById(R.id.bt_back_topic, this);

        rvStory = findViewById(R.id.rv_favorite);
        rvStory.setLayoutManager(new LinearLayoutManager(this));

        TextView tvTopicName = findViewById(R.id.tv_topic_actionbar);
        tvTopicName.setText("Truyện yêu thích");

        listStory = StoryContent.listFavorite;
        rvAdapter = new StoryRVAdapter(this, listStory, this);

        rvStory.setAdapter(rvAdapter);
    }

    private void fillData(View itemView, final int i) {
        TextView tvNameStory = itemView.findViewById(R.id.tv_story_name);
        ImageView ivRemove = itemView.findViewById(R.id.iv_remove);
        ivRemove.setVisibility(View.VISIBLE);
        ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.iv_remove) {
                    StoryContent.listFavorite.remove(i);
                }
            }
        });

        tvNameStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getInstance().setStoryPosition(i);
                startActivity(new Intent(ListFavoriteStory.this, StoryContent.class));
                Toast.makeText(ListFavoriteStory.this, i + " : " + pos, Toast.LENGTH_SHORT).show();
            }
        });
        tvNameStory.setText(StoryContent.listFavorite.get(i).getName());

    }


    @Override
    protected void clickView(int id) {
        if (id == R.id.bt_back_topic) {
//            startActivity(new Intent(this, MainStoryAct.class));
            finish();
        }
    }


    @Override
    public void clickItemRV(StoryEntity story) {
        listStory.remove(story);
        rvAdapter.notifyDataSetChanged();
    }

    @Override
    public void toStoryContent() {

    }
}
