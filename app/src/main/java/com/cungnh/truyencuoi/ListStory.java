package com.cungnh.truyencuoi;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cungnh.truyencuoi.entity.StoryEntity;

import java.util.ArrayList;
import java.util.List;

public class ListStory extends BaseActivity {
    private LinearLayout lnListStory;
    private List<StoryEntity> listStory;
    private int pos;
    public static final String POS_STORY = "positionStory";

    @Override
    protected int getLayoutId() {
        return R.layout.list_story;
    }

    @Override
    protected void initView() {

        initV(pos);


//        findViewById(R.id.bt_back_topic, this);
//
//        pos = App.getInstance().getTopicPosition();
//        lnListStory = findViewById(R.id.ln_list_story);
//        lnListStory.removeAllViews();
//        TextView tvTopicName = findViewById(R.id.tv_topic_actionbar);
//        tvTopicName.setText(MainStoryAct.NAME_TOPIC[pos]);
//
//
//        listStory = new ArrayList<>();
//        listStory = CommonUtils.getInstance().getStory(pos);
////        Log.i("ListStory : ", "size: " + listStory.size());
//
//        for (int i = 0; i < listStory.size(); i++) {
//            View itemView = LayoutInflater.from(this).inflate(R.layout.story_name, null);
//            fillData(itemView, i);
//            lnListStory.addView(itemView);
//        }
    }

    private void initV(int pos) {
        findViewById(R.id.bt_back_topic, this);

        pos = App.getInstance().getTopicPosition();
        lnListStory = findViewById(R.id.ln_list_story);
        lnListStory.removeAllViews();
        TextView tvTopicName = findViewById(R.id.tv_topic_actionbar);
        tvTopicName.setText(MainStoryAct.NAME_TOPIC[pos]);


        listStory = new ArrayList<>();
        listStory = CommonUtils.getInstance().getStory(pos);
//        Log.i("ListStory : ", "size: " + listStory.size());

        for (int i = 0; i < listStory.size(); i++) {
            View itemView = LayoutInflater.from(this).inflate(R.layout.story_name, null);
            fillData(itemView, i);
            lnListStory.addView(itemView);
        }
    }

    private void fillData(View itemView, final int i) {
        TextView tvNameStory = itemView.findViewById(R.id.tv_story_name);
        tvNameStory.setText(listStory.get(i).getName());
        itemView.findViewById(R.id.ln_story_name).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                App.getInstance().setStoryPosition(i);
                startActivity(new Intent(ListStory.this, StoryContent.class));
                Toast.makeText(ListStory.this,  i + " : " + pos, Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    protected void clickView(int id) {
        if (id == R.id.bt_back_topic) {
//            startActivity(new Intent(this, MainStoryAct.class));
            finish();
        }
    }


//    @Override
//    protected void onStart() {
//        super.onStart();
//        int pos = App.getInstance().getStoryPosition();
//        initV(pos);
//    }
}
