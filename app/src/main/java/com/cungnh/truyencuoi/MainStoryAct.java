package com.cungnh.truyencuoi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.drawerlayout.widget.DrawerLayout;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class MainStoryAct extends BaseActivity implements View.OnClickListener {
    public static final String[] NAME_TOPIC = new String[]{"Con gái", "Công sở",
            "Con nít", "Con trai",
            "Cực hài", "Cười 18", "Dân gian", "Gia đình", "Giao thông"};
    public static final String[] IMG_TOPIC = new String[]{"congai", "congso",
            "connit", "contrai",
            "cuchai", "cuoi18", "dangian", "giadinh", "giaothong"};
    private static final String TAG = "My Application";
    private static final String NAME_POSITION = "POSITION";
    private boolean isAppend;
    private int position;
    private ImageView ivStory;
    private TextView tvStory;
    private DrawerLayout drLayout;

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        for (int i = 0; i < grantResults.length; i++) {
//            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
//                finish();
//                return;
//            }
//        }
//    }

    @Override
    protected int getLayoutId() {
        return R.layout.main_story;
    }

    @Override
    protected void initView() {

//        if (CommonUtils.getInstance().getIsSave()) {
//            startActivity(new Intent(MainStoryAct.this, StoryContent.class));
//        }

        drLayout = findViewById(R.id.drawer_layout);
        findViewById(R.id.tb_favorite, this);
        findViewById(R.id.tb_info, this);
        findViewById(R.id.tb_language, this);
        findViewById(R.id.tb_save, this);
        findViewById(R.id.iv_menu, this);
        LinearLayout lnMain = findViewById(R.id.ln_main);
        lnMain.removeAllViews();

        for (int i = 0; i < NAME_TOPIC.length; i++) {
            View itemView = LayoutInflater.from(this).inflate(R.layout.story_topic, null);
            fillData(itemView, i);
            lnMain.addView(itemView);
        }

    }

    private void fillData(View itemView, final int pos) {
        ivStory = itemView.findViewById(R.id.iv_story_topic);
        tvStory = itemView.findViewById(R.id.tv_story_topic);

        tvStory.setText(NAME_TOPIC[pos]);
        itemView.findViewById(R.id.ln_story_topic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getInstance().setTopicPosition(pos);

                startActivity(new Intent(MainStoryAct.this, ListStory.class));
                Toast.makeText(MainStoryAct.this, "Img: " + IMG_TOPIC[pos] + " pos: " + pos, Toast.LENGTH_SHORT).show();
            }
        });

        try {

            InputStream in = getAssets().open("icon/vn/" + IMG_TOPIC[pos] + ".png");
            Bitmap img = BitmapFactory.decodeStream(in);
            ivStory.setImageBitmap(img);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void clickView(int id) {
        if (id == R.id.iv_menu) {
            drLayout.openDrawer(Gravity.LEFT);
        } else if (id == R.id.tb_save) {
            drLayout.closeDrawers();
            startActivity(new Intent(this, SaveStory.class));
            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.tb_favorite) {
            drLayout.closeDrawers();
            startActivity(new Intent(this, ListFavoriteStory.class));
            Toast.makeText(this, "List favorite story", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.tb_language) {
            Toast.makeText(this, "Language", Toast.LENGTH_SHORT).show();
        }
    }

    private void writeFileExternalStorage() {
//        String path = getFilesDir().getPath();
//        Tro vao vung shared storage
        String path1 = Environment.getExternalStorageDirectory().getPath();


//       //         Tro vao vung second storage danh rieng cho moi app
//        String path1 = getExternalFilesDir(null).getPath();

        File file = new File(path1 + "/content1.txt");
        int size = (int) file.length();

        try {
            RandomAccessFile rf = new RandomAccessFile(file, "rw");
            rf.seek(rf.length());
            rf.writeBytes("edf");
            rf.close();
            Toast.makeText(this, "File is written", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(TAG, "File name : " + file.getName());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
