package com.cungnh.truyencuoi;

import android.content.Intent;
import android.os.Handler;

public class SplashAct extends BaseActivity {

    @Override
    protected void initView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doTaskDelay();
            }
        }, 2000);

    }

    private void doTaskDelay() {
        startActivity(new Intent(this, MainStoryAct.class));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.splash;
    }
}
