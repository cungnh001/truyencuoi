package com.cungnh.truyencuoi;

import android.app.Application;

public class App extends Application {
    private  static App instance;
    private int topicPosition, storyPosition;
    private  String nameImg ;

    public static  App getInstance(){
        return  instance;
    }

    public void setTopicPosition(int pos){
        topicPosition = pos ;
    }

    public int getTopicPosition(){
        return topicPosition;
    }

    public void setNameImg(String name){
        this.nameImg = nameImg;
    }

    public String getNameImg(){
        return nameImg;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public void setStoryPosition(int storyPosition) {
        this.storyPosition = storyPosition;
    }

    public int getStoryPosition() {
        return storyPosition;
    }
}
